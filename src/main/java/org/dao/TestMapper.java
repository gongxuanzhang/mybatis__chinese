package org.dao;


import org.Test;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author gxz
 * @date 2020/5/12 23:11
 */
@Mapper
public interface TestMapper {
    Test findOne(int id);

    int save(Test test);
}
