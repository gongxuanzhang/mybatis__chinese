package org;


import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.dao.TestMapper;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author gxz
 * @date 2020/5/12 23:08
 */
public class App {
  public static void main(String[] args) throws IOException {


  }

  @Test
  public void aaa() throws IOException {
    String resource = "org/mybatis-config.xml";
    // 读取配置文件
    InputStream inputStream = Resources.getResourceAsStream(resource);
    // 构建sqlSessionFactory
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    SqlSession  sqlSession = sqlSessionFactory.openSession(true);
    SqlSession  sqlSession1 = sqlSessionFactory.openSession(true);
      // 指定全局配置文件
      // 获取sqlSession

      // 操作CRUD，第一个参数：指定statement，规则：命名空间+“.”+statementId
      // 第二个参数：指定传入sql的参数：这里是用户id
      TestMapper mapper = sqlSession.getMapper(TestMapper.class);
      TestMapper mapper1 = sqlSession1.getMapper(TestMapper.class);
      org.Test one = mapper.findOne(4);
      sqlSession.commit();
      System.out.println(one);
      org.Test one1 = mapper1.findOne(4);
      System.out.println(one1);


  }
}
