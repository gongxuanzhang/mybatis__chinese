package org;


import lombok.Data;

import java.io.Serializable;

/**
 * @author gxz
 * @date 2020/5/12 23:06
 */
@Data
public class Test implements Serializable {
    private String name;
    private Integer id;
    private String test;

    public Test setName(String name) {
        this.name = name;
        return this;
    }

    public Test setId(Integer id) {
        this.id = id;
        return this;
    }

    public Test setTest(String test) {
        this.test = test;
        return this;
    }
}
